# Copyright 2018 Rasmus Thomsen <Rasmus.thomsen@protonmail.com>
# Distributed under the terms of the GNU General Purpose License v2

# Meson's kind of broken ( doesn't link correctly to app-pim/tracker )
require gnome.org [ suffix=tar.xz ]
require vala [ vala_dep=true ]
require gsettings test-dbus-daemon

SUMMARY="Collection of data extractors for Tracker"

LICENCES="GPL-2 LGPL-2.1"
SLOT="2.0"
PLATFORMS="~amd64 ~x86"

MYOPTIONS="
    cuesheet [[ description = [ enable external cuesheet parsing ] ]]
    exif
    flac [[ description = [ enable flac data extraction ] ]]
    gif [[ description = [ enable gif data extraction ] ]]
    gstreamer [[ description = [ use gstreamer for media extraction ] ]]
    jpeg [[ description = [ enable jpeg data extraction ] ]]
    ms-office [[ description = [ enable .doc(x) data extraction ] ]]
    pdf [[ description = [ enable PDF data extraction ] ]]
    playlist [[ description = [ enable playlist data extraction ] ]]
    upower [[ description = [ support battery/power detection ] ]]
    raw [[ description = [ enable RAW data extraction ] ]]
    taglib [[ description = [ enable writeback for audio files ] ]]
    tiff [[ description = [ enable tiff data extraction ] ]]
    vorbis [[ description = [ enable vorbis data extraction ] ]]
    xmp [[ description = [ enable XMP data extraction ] ]]
    ( linguas: ar be@latin ca ca@valencia cs da de dz el en_GB eo es et fi fr gl he hu id it ja ko
               lt lv mk ml nb nds nl oc pa pl pt pt_BR ro ru sk sl sr sr@latin sv te th zh_CN zh_HK
               zh_TW )
    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.40.0]
        sys-devel/gettext
        virtual/pkg-config
    build+run:
        app-pim/tracker:2.0[>=1.99]
        dev-libs/glib:2[>=2.40.0]
        dev-libs/icu:=[>4.8.1.1]
        dev-libs/libxml2:2.0[>=2.6]
        gnome-desktop/gobject-introspection:1
        media-libs/libexif[>0.6]
        media-libs/libpng:=[>=1.2]
        sys-apps/dbus[>=1.3.1]
        sys-libs/libseccomp[>=2.0]
        sys-libs/zlib
        cuesheet? ( media-libs/libcue )
        exif? ( media-libs/libexif[>=0.6] )
        flac? ( media-libs/flac[>=1.2.1] )
        gif? ( media-libs/giflib:= )
        gstreamer? (
            media-libs/gstreamer:1.0
            media-plugins/gst-plugins-base:1.0
        )
        ms-office? ( office-libs/libgsf:1[>=1.14.24] )
        pdf? (
            app-text/poppler[glib][>=0.16.0]
            dev-libs/libgxps
        )
        playlist? ( gnome-desktop/totem-pl-parser )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        raw? ( dev-libs/gexiv2 )
        taglib? ( media-libs/taglib[>=1.6] )
        tiff? ( media-libs/tiff )
        upower? ( sys-apps/upower[>=0.9.0] )
        vorbis? ( media-libs/libvorbis[>=0.22] )
        xmp? ( media-libs/exempi:2.0[>=2.1.0] )
        upower? ( sys-apps/upower[>=0.9.0] )
    test:
        dev-lang/python:2.7
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --enable-dvi
    --enable-icon
    --enable-icu-charset-detection
    --enable-libexif
    --enable-libjpeg
    --enable-libpng
    --enable-libxml2
    --enable-miner-apps
    --enable-miner-fs
    --enable-mp3
    --enable-playlist
    --enable-ps
    --enable-text
    --enable-tracker-writeback

    # unpackaged libgrss
    --disable-miner-rss

    --disable-abiword
    --disable-installed-tests

    --with-gstreamer-backend=discoverer
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=(
    'cuesheet libcue'
    'exif libexif'
    'flac libflac'
    'gif libgif'
    'gstreamer generic-media-extractor gstreamer'
    'ms-office libgsf'
    'pdf poppler'
    'pdf libgxps'
    'playlist'
    'raw gexiv2'
    'taglib'
    'tiff libtiff'
    'upower'
    'vorbis libvorbis'
    'xmp exempi'
)

DEFAULT_SRC_CONFIGURE_TESTS=(
    '--enable-functional-tests --disable-functional-tests'
)

src_test() {
    unset DISPLAY

    # redirect any configuration and user settings to temp via XDG variables
    export XDG_DATA_HOME=${TEMP}
    export XDG_CACHE_HOME=${TEMP}
    export XDG_CONFIG_HOME=${TEMP}

    # G_DBUS_COOKIE_SHA1_KEYRING_DIR requires 0700, ${TEMP} is 0755
    export G_DBUS_COOKIE_SHA1_KEYRING_DIR_IGNORE_PERMISSION=1
    export G_DBUS_COOKIE_SHA1_KEYRING_DIR=${TEMP}

    # use the memory backend for settings to ensure that the system settings in dconf remain
    # untouched by the tests
    export GSETTINGS_BACKEND=memory

    # the tracker-dbus/request test relies on g_debug() messages being output to stdout
    export G_MESSAGES_DEBUG=all

    test-dbus-daemon_start

    TZ=UTC LANG=C LC_ALL=C HOME="${TEMP}" test-dbus-daemon_run-tests
    success=${?}

    test-dbus-daemon_stop
}

src_install() {
    default

    # Seems to me as if the disable-installed-tests switch doesn't work
    edo rm -r ${IMAGE}/usr/share/tracker-tests
}

